

scalable-protocol

v0.1/2015-01-29



1 protocol format

1.1 whole protocol

    message identification + reserved region + header length + body length + header + body

1.2 prefix

    message identification + reserved region
    
    message identification: assici string, 6Bytes. for example, "1A2B3C".
    
    reserved region: set of fixed fields. char (2Bytes schema map) and byte stream (n1 Bytes). 
            for example, protocol version, has header length, has body length, checksum, direction(c->s,s->c,c->c,s->s), packageid, uncompressed size, decrypted size.

1.3 suffix

    header length + body length + header + body
    
    header length: int of network-endian (4Bytes).
    
    body length: int of network-endian (4Bytes).
    
    header: byte stream (n2 Bytes, byte stream, variable).
    
    body: byte stream (n3 Bytes, byte stream, variable).

    header example: project unique flag, msgtype, from/to channelid, json/protobuf/thrift format.

1.4 total message length

    6B+2B+n1B+4B+4B+n2B+n3B=16+n1+n2+n3
    
1.5 heartbeat message

    heartbeat message(8Bytes) : "1A2B3C\0\0" , minimum message length. 

1.6 receiving process

    read 8 bytes
    
    read n1 bytes
    
    read 8 bytes
    
    read n2+n3 bytes

1.7 splite and merge

    small message need user to merge , large message need user to splite.
    



