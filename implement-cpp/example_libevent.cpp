

enum NetworkRecvingStatus
{
    NRS_WAIT_PREFIX0,
    NRS_WAIT_PREFIX1,
    NRS_WAIT_SUFFIX0,
    NRS_WAIT_SUFFIX1    
};

class Session
{
};

static int read_prefix0(bufferevent *bev, Session * sess) // read 8 bytes, message identification + 1Byte
{
}

static int read_prefix1(bufferevent *bev, Session * sess) // read n1 bytes, reserved region
{
}

static int read_suffix0(bufferevent *bev, Session * sess) // read 8 bytes, left length + header length
{
}

static int read_suffix1(bufferevent *bev, Session * sess) // n2+n3 bytes, header + body
{
}

static int read_next_segment(bufferevent *bev, Session * sess)
{
}

// message identification + reserved region + left length + header length + header + body
static void readcb(bufferevent *bev, void *ctx)
{
    CHECK_NULL(bev)
    Session * sess =(Session *)ctx;
    CHECK_NULL(sess)
    
    while(evbuffer_get_length(bufferevent_get_input(bev))>0)
    {
        if(-1==read_next_segment(bev,sess))
            break;
    }
}
