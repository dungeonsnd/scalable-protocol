
#include "sproto_parser.hpp"


#define SPROTO_PARSER_NH_SWAP(A)    \
( (((unsigned)(A) & 0xff000000) >> 24) | (((unsigned)(A) & 0x00ff0000) >> 8) | \
  (((unsigned)(A) & 0x0000ff00) << 8) | (((unsigned)(A) & 0x000000ff) << 24) )

             
namespace sp
{

SProtoParser::SProtoParser()
{
}

SProtoParser::~SProtoParser()
{
}

int SProtoParser::GetSizeOfPrefix0()
{
    return 8;
}

int SProtoParser::GetSizeOfPrefix1(const char * prefix0,int len)
{
    int res =-1;
    do{
        if(len<8)
        {
            break;
        }
        
        res =prefix0[7];
    }while(0);
    return res;
}

int SProtoParser::GetSizeOfSuffix0()
{
    return 8;
}

int SProtoParser::GetSizeOfSuffix1(const char * suffix0,int len)
{
    int res =-1;
    do{
        if(len<8)
        {
            break;
        }
        
        const int * t =(const int *)suffix0;
        res =SPROTO_PARSER_NH_SWAP(*t);
    }while(0);
    return res;
}


} // namespace sp

