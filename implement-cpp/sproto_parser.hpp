#ifndef _HEADER_FILE_SPROTO_PARSER_HPP_
#define _HEADER_FILE_SPROTO_PARSER_HPP_

namespace sp
{

enum SProtoRecvingStatus
{
    SPRS_WAIT_PREFIX0, // read 8 bytes
    SPRS_WAIT_PREFIX1, // read n1 bytes
    SPRS_WAIT_SUFFIX0, // read 8 bytes
    SPRS_WAIT_SUFFIX1  // read n2+n3 bytes
};

class SProtoParser
{
public:
    SProtoParser();
    ~SProtoParser();
    
    // return 8
    int GetSizeOfPrefix0();
    
    // return n1
    int GetSizeOfPrefix1(const char * prefix0,int len);
    
    // return 8
    int GetSizeOfSuffix0();
    
    // return n2+n3
    int GetSizeOfSuffix1(const char * suffix0,int len);
};

} // namespace sp

#endif // _HEADER_FILE_SPROTO_PARSER_HPP_

